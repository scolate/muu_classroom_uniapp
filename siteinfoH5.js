// H5端执行 获取siteInfo配置数据

function getParamsH5(){
	var url = window.location.href;                 //获取当前url
    var dz_url = url.split('#')[0];                //获取#/之前的字符串
    //var siteroot = dz_url.split('?')[0];
	var siteroot = window.location.protocol+"//"+window.location.host+"/app/index.php";
	var cs = dz_url.split('?')[1];             //获取?之后的参数字符串
		cs = (cs.substring(cs.length - 1) == '/') ? cs.substring(0, cs.length - 1) : cs;
	if(cs){
		var cs_arr = cs.split('&');                    //参数字符串分割为数组
	    var cs={};
		for(var i=0;i<cs_arr.length;i++){         //遍历数组，拿到json对象
			cs[cs_arr[i].split('=')[0]] = cs_arr[i].split('=')[1]
	    }
	}else{
		
	}
	
	cs.siteroot = siteroot
	cs.uniacid = cs.i
	console.log(cs.uniacid,'cs uniacid')
	return cs
};

//生产环境
 var urlParamsH5 = getParamsH5();
 var siteInfo = urlParamsH5;

//开发环境
/*
var siteInfo = {
	uniacid: 2, //uniacid
	siteroot: 'http://demo.w7.muucmf.cn/app/index.php',  //站点URL
};
*/

module.exports = siteInfo;
