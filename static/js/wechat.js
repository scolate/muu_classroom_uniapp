import util from './util.js'
var jweixin = require('jweixin-module')

export default {
    //判断是否在微信中  
    isWechat: function() {
        var ua = window.navigator.userAgent.toLowerCase();
        if (ua.match(/micromessenger/i) == 'micromessenger') {
            // console.log('是微信客户端')
            return true;
        } else {
            // console.log('不是微信客户端')
            return false;
        }
    },
    //初始化sdk配置  
    initJssdk: function(callback, url) {
		url = url ? url : window.location.href;
        //console.log("init Url : "+url)
        // 这是我这边封装的 request 请求工具，实际就是 uni.request 方法。
		let all_url_arr = url.split('#')
			url = all_url_arr[0]
		util.request({
			url : 'entry/site/jssdk',
			data: {
				'url' : url
			},
			success: function (res) {
				console.log(res.data)
				if (res.data.code == 200) {
					let result = res.data.data;
					
					jweixin.config({
					    debug: false,
					    appId: result.appId,
					    timestamp: result.timestamp,
					    nonceStr: result.nonceStr,
					    signature: result.signature,
					    jsApiList: [
					        'checkJsApi',
					        'onMenuShareTimeline',
					        'onMenuShareAppMessage',
							'scanQRCode',
							'chooseWXPay',
					    ]
					});
					//配置完成后，再执行分享等功能  
					if (callback) {
					    callback(result);
					}
				}
			}
		})
    },
    //在需要自定义分享的页面中调用  
    share: function(data, url) {
        url = url ? url : window.location.href
		//console.log(url)
		let all_url_arr = url.split('#')
			url = all_url_arr[0]

		let share_link = this.shareUrl()
		//console.log(share_link)
        if (!this.isWechat()) {
            return;
        }
        //每次都需要重新初始化配置，才可以进行分享  
        this.initJssdk(function(signData) {
            jweixin.ready(function() {
				console.log('jweixin ready')
                var shareData = {
                    title: data && data.title ? data.title : signData.site_name,
                    desc: data && data.desc ? data.desc : signData.site_description,
                    link: share_link,
                    imgUrl: data && data.img ? data.img : signData.site_logo,
                    success: function(res) {
                        // 分享后的一些操作,比如分享统计等等
                    },
                    cancel: function(res) {}
                };
                //分享给朋友接口  
                jweixin.onMenuShareAppMessage(shareData);
                //分享到朋友圈接口  
                jweixin.onMenuShareTimeline(shareData);
            });
        }, url);
    },
	
	pay: function(payObj, url){
		url = url ? url : window.location.href;
		console.log("url:"+url)
		if (!this.isWechat()) {
		    return;
		}
		//每次都需要重新初始化配置，才可以进行分享  
		this.initJssdk(function(signData){
		    jweixin.ready(function() {
				console.log('jweixin ready')
				// 发起支付
		        jweixin.chooseWXPay({
					timestamp: payObj.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
					nonceStr: payObj.nonceStr, // 支付签名随机串，不长于 32 位
					package: payObj.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
					signType: 'MD5', // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
					paySign: payObj.paySign, // 支付签名
					success:function(res) {
						// 支付成功后的回调函数
						payObj.success(res)
					},
					cancel: function(res) {
						console.log(res)
					},
					fail:function(res) {
						console.log(res)
						payObj.fail(res)
					}
				});
		    });
		}, url);
	},
	
	/**
	 * 自定义分享网址
	 **/
	shareUrl:  function() {
		let url = window.location.href
		let url_arr = url.split('#');
		let spa_url_param = encodeURIComponent(url_arr[1])
		// 分享的URL地址
			url = url_arr[0] + '&spa_param='+ spa_url_param + '#' + url_arr[1]
		// 获取分销商UID	
		let userInfo = uni.getStorageSync('userInfo');
		let memberInfo = ''
		if(userInfo){
			memberInfo = userInfo.memberInfo
		}
		
		if(memberInfo && memberInfo.uid !== undefined){
			url = url + '&promoter_uid=' + memberInfo.uid
		}
	
		return url;
	}
}