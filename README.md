
# muu_classroom_uniapp

#### 介绍
Muu云课堂V2/uniapp是Muu云课堂V2整套前端开源代码。
   Muu云课堂V2是一款全场景付费音视频内容管理系统，支持知识付费、在线教育、企业内容等诸多场景。DIY心中理想的店铺样式，制作专属新知识付费平台。支持图文、音频、视频、直播等各种主流媒体形式，店主可以通过公众号、小程序、PC端、H5等各种渠道宣传，引流客户访问店铺，宣传店铺内在线知识产品。

   经过一年多的产品打磨和更新迭代，Muu云课堂V2的功能几乎可以满足各类知识付费用户的需求，系统商用稳定，加上无限多开版的强大优势，现已拥有众多忠实客户。

   应很多有二开能力老用户的强烈要求，也是为了答谢广大用户对我们的支持和信任，现将uniapp开发的整套Muu云课堂前端代码全部开源，提供给有二开能力的开发者们使用。如果您喜欢我们这套系统，也欢迎入手。详情介绍请见：https://s.w7.cc/module-24412.html

![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/141957_d6d757e2_378490.jpeg "1-头图.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/133922_c7ef118f_378490.png "001.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/133932_8c21bfc3_378490.png "002.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/133944_860d4886_378490.png "003.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134014_e896ee0b_378490.png "01.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134028_3cea62f4_378490.png "02.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134037_d8dc1270_378490.png "03.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134059_e045ec9f_378490.png "02-1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134108_6c2a1942_378490.png "02-2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134128_1b079876_378490.png "02-3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134142_7928112b_378490.png "0--1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134152_142d56ba_378490.png "0--2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134207_18818b43_378490.png "03-1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134216_adeb65f2_378490.png "03-2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134236_57605d32_378490.png "PC-01.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134246_01befcb3_378490.png "pc-02.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134254_ed9090fe_378490.png "pc-03.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134309_aee05fbb_378490.png "pc-2-1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134320_671ab27b_378490.png "pc-2-2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/134329_2c8858ed_378490.png "pc-2-3.png")



